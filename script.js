
// Constants for Nostr relay
const NOSTR_RELAY_URL = 'wss://relay.example.com'; // Replace with an actual Nostr relay URL
let nostrSocket = null;

// Function to establish WebSocket connection
function connectToNostrRelay() {
    nostrSocket = new WebSocket(NOSTR_RELAY_URL);

    nostrSocket.onopen = function() {
        console.log('Connected to Nostr relay');
        // Handle successful connection
    };

    nostrSocket.onmessage = function(event) {
        // Handle incoming messages (notes)
        const data = JSON.parse(event.data);
        if (data && data.type === 'note') {
            displayNote(data.content);
        }
    };

    nostrSocket.onerror = function(error) {
        console.error('WebSocket Error:', error);
    };

    nostrSocket.onclose = function() {
        console.log('Disconnected from Nostr relay');
        // Attempt to reconnect
        setTimeout(connectToNostrRelay, 5000);
    };
}

// Function to submit note to Nostr relay
function submitNote() {
    const note = noteContent.value;
    const nostrEvent = {
        // Define the event format according to Nostr specifications
        type: 'note',
        content: note
    };

    // Send the event to the relay
    nostrSocket.send(JSON.stringify(nostrEvent));
    noteContent.value = '';
}

// Event listener for form submission
submitForm.addEventListener('submit', function(event) {
    event.preventDefault();
    submitNote();
});

// Function to display notes
function displayNote(note) {
    const noteElement = document.createElement('div');
    noteElement.innerText = note;
    notesContainer.appendChild(noteElement);
}

// Connect to the Nostr relay on load
window.onload = connectToNostrRelay;
